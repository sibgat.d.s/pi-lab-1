import React from 'react';
import { Router, Route, Switch } from "react-router-dom";

import HomePage from '../pages/home';
import FirstPage from '../pages/first-page';
import CtpCalculationPage from '../pages/ctp-calculation-page';
import WorkingCalendarPage from '../pages/working-calendar-page';
import DevelopmentIndexPage from "../pages/development-index-page";
import WorkerRatePage from "../pages/worker-rate-page";

import {
    History,
} from 'utilities';

class BrowserRouter extends React.Component {

    render () {
        return (
            <Router history={History}>
                <Switch>
                    <Route exact path='/' component={HomePage} />
                    <Route exact path='/first-page' component={FirstPage} />
                    <Route exact path='/ctp-calculation-page' component={CtpCalculationPage} />
                    <Route exact path='/working-calendar' component={WorkingCalendarPage} />
                    <Route exact path='/development-index' component={DevelopmentIndexPage} />
                    <Route exact path='/development-index/worker-rate' component={WorkerRatePage} />
                </Switch>
            </Router>
        )
    }

}

export default BrowserRouter;