import {computed, observable} from "mobx";
import lodash from 'lodash';
import moment from 'moment';

class Store {
    // Стоимость проектирования
    @observable ProectingCosts = '';
    // Стоимость реализации
    @observable RealiseCosts = '';
    // выбранные показатели качества
    @observable QIRealised = [];
    // коэффициент технической обоснованности разработки
    @observable CTP = '';
    // Показатели качества
    @observable qualityIndicators = [
        {
            id: '1',
            name: 'Удобство работы (пользовательский интерфейс)',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '2',
            name: 'Новизна (соответствие современным требованиям)',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '3',
            name: 'Соответствие профилю деятельности заказчика',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '4',
            name: 'Ресурсная эффективность',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '5',
            name: 'Надежность (защита данных)',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '6',
            name: 'Скорость доступа к данным',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '7',
            name: 'Гибкость настройки',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '8',
            name: 'Обучаемость персонала',
            value: 0,
            isNeeded: 0,
        },
        {
            id: '9',
            name: 'Соотношение стоимость/возможности',
            value: 0,
            isNeeded: 0,
        },
    ];

    CoefsToCalculate = {
        workersCount: this.executers.length,

    };

    // Исполнители заказа
    @observable executers = [
        {
            name: 'руководитель',
            rate: 904.76,
            id: 0,
        },
        {
            name: 'программист',
            rate: 333.33,
            id: 1,
        }
    ];

    // выбор показателя качества по 'id'
    @computed
    get QIRealisedById() {
        return lodash.keyBy(this.QIRealised, 'id');
    }

    // вычисления дня начала работы
    @computed
    get startProjectDate() {
        if (moment().isoWeekday() === 7) {
            return moment().add(1, 'days');
        }
        if (moment().isoWeekday() === 6) {
            return moment().add(2, 'days');
        }
        return moment();
    }
}

const store = new Store();
export default store;