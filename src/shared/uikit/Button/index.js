import React from 'react';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

const Button = ({children, onClick, className, to}) => {
    const classNames = cn('btn', className);

    if (to) {
        return (
            <NavLink to={to} className={classNames} onClick={onClick}>
                {children}
            </NavLink>
        );
    }

    return (
        <button onClick={onClick} className={classNames}>
            {children}
        </button>
    );
};

Button.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    className: PropTypes.any,
    to: PropTypes.string,
};

Button.defaultProps = {
    children: '',
    className: '',
    onClick: () => {},
    to: null
};

export default Button;