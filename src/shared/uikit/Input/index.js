import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

class Input extends Component {
    static propTypes = {
        id: PropTypes.any,
        type: PropTypes.string,
        name: PropTypes.string,
        className: PropTypes.any,
        placeholder: PropTypes.string,
        value: PropTypes.any,
        autofocus: PropTypes.bool,
        required: PropTypes.bool,
        label: PropTypes.string,
        description: PropTypes.string,
        onChange: PropTypes.func,
    };

    static defaultProps = {
        type: 'text',
        name: 'name',
        className: '',
        placeholder: '',
        value: '',
        id: '',
        autofocus: false,
        required: false,
        label: null,
        description: null,
        onChange: () => {},
    };

    handleChange = (e) => {
        const {
            id,
            name,
            type,
            onChange
        } = this.props;

        onChange({
            name: name,
            type: type,
            value: e.target.value,
            id: id,
        })
    };

    get input() {
        const {
            type,
            id,
            name,
            className,
            placeholder,
            value,
            autofocus,
            required,
        } = this.props;

        const classNames = cn('form-control', className);
        return (
            <input
                id={id}
                type={type}
                name={name}
                className={classNames}
                placeholder={placeholder}
                value={value}
                autoFocus={autofocus}
                required={required}
                onChange={this.handleChange}
            />
        )
    }

    get label() {
        if (this.props.label) {
            return (
                <label>{this.props.label}</label>
            )
        }

        return null;
    }

    render () {
        return (
            <Fragment>
                <div className="m-1">
                    {this.label}
                    {this.input}
                </div>
            </Fragment>
        );
    }
}

export default Input;