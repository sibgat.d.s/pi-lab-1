import React from 'react';
import {FaQuestionCircle} from 'react-icons/fa';
import cn from 'classnames';
import PropTypes from 'prop-types';

const Tooltip = ({text, className, placement, }) => {
    const classnames = cn('btn', className);
    return (
        <button type='button' className={classnames} dataToggle="tooltip" dataPlacement={placement} title={text}>
            <FaQuestionCircle/>
        </button>
    )
};

Tooltip.propTypes = {
    text: PropTypes.string,
    className: PropTypes.any,
    placement: PropTypes.string,
};

Tooltip.defaultProps = {
    text: '',
    className: null,
    placement: 'top',
};

export default Tooltip;