import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

const Card = ({className, children, label}) => {
    const classNames = cn('card', 'px-2', className);

    return (
        <div className={classNames}>
            <label>{label}</label>
            <div className="card-body">
                {children}
            </div>
        </div>
    );
};


Card.propTypes = {
    label: PropTypes.string,
    children: PropTypes.any,
    className: PropTypes.any,
};

Card.defaultProps = {
    label: '',
    children: '',
    className: '',
};

export default Card;