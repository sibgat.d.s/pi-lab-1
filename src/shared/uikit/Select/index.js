import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import lodash from 'lodash';
import { computed, action } from "mobx";

class Select extends Component {
    static propTypes = {
        id: PropTypes.any,
        options: PropTypes.any,
        name: PropTypes.string,
        className: PropTypes.any,
        placeholder: PropTypes.string,
        value: PropTypes.any,
        autofocus: PropTypes.bool,
        required: PropTypes.bool,
        label: PropTypes.string,
        description: PropTypes.string,
        onChange: PropTypes.func,
        isEmpty: PropTypes.bool,
    };

    static defaultProps = {
        options: [],
        name: 'name',
        className: '',
        placeholder: '',
        value: '',
        id: '',
        autofocus: false,
        required: false,
        label: null,
        description: null,
        onChange: () => {
        },
        isEmpty: true,
    };

    @action
    handleChange = (e) => {
        const {
            id,
            name,
            onChange
        } = this.props;

        onChange({
            name: name,
            value: this.optionById[e.target.value].id,
            id: id,
        })
    };

    @computed
    get optionById() {
        return lodash.keyBy(this.props.options, 'id')
    }

    @computed
    get select() {
        const {
            options,
            id,
            name,
            className,
            placeholder,
            value,
            autofocus,
            required,
        } = this.props;

        const classNames = cn('form-control', className);
        return (
            <select
                id={id}
                name={name}
                className={classNames}
                placeholder={placeholder}
                value={value}
                autoFocus={autofocus}
                required={required}
                onChange={this.handleChange}
            >
                {options.map(item => {
                    return (
                        <option key={item.id} value={item.id}>{item.name}</option>
                    )
                })}
            </select>
        )
    }

    @computed
    get label() {
        if (this.props.label) {
            return (
                <label>{this.props.label}</label>
            )
        }

        return null;
    }

    render() {
        return (
            <Fragment>
                <div className="m-1">
                    {this.label}
                    {this.select}
                </div>
            </Fragment>
        );
    }
}

export default Select;