import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

const Checkbox = ({name, className, onClick, value, label}) => {
    const classNames = cn('custom-control', 'custom-checkbox', 'mr-sm-2' , className);

    const handleClick = () => {
        onClick({
            value: (value == 1 || value == true) ? (0) : 1,
            name: name,
            type: 'checkbox',
        })
    };

    return (
        <div className={classNames} onClick={handleClick}>
            <input
                type="checkbox"
                name={name}
                checked={value}
                className="custom-control-input"
                onChange={()=>{}}
            />
            <label className="custom-control-label" htmlFor="customControlAutosizing">{label}</label>
        </div>
    );
};


Checkbox.propTypes = {
    name: PropTypes.any,
    className: PropTypes.any,
    onClick: PropTypes.func,
    value: PropTypes.any,
    label: PropTypes.string,
};

Checkbox.defaultProps = {
    name: 'checkbox',
    className: '',
    onClick: () => {},
    value: false,
    label: '',
};

export default Checkbox;