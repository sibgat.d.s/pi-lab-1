import lodash from 'lodash';

import {CountStore} from 'stores';

class Service {
    handleChangeCountStore = (e) => {
        CountStore[e.name] = e.value;
    };

    handleChangeQualityIndicators = (e) => {
        CountStore.qualityIndicators[e.name].isNeeded = e.value;
    };


    handleChangeQIRealised = (e) => {
        CountStore.QIRealisedById[e.id][e.name] = e.value;
        if (e.name === 'value') {
            CountStore.QIRealised = this.indexRecounting(CountStore.QIRealised, e.id);
        }
    };

    indexRecounting = (qiArray, index) => {
        const arrLength = qiArray.length;
        const upperSum = lodash.sumBy(qiArray, (item) => {
            if (+item.id <= index) {
                return +item.value
            }
            return 0;
        });
        const downSum = 1 - upperSum;
        return qiArray.map(item => {
            if (+item.id > index) {
                item.value = Math.round(downSum / (arrLength - index - 1) * 100) / 100;
            }
            return item;
        })
    };
}

const service = new Service();
export default service;