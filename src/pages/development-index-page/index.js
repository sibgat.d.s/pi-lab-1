import React from 'react';

import {
    Button
} from 'uikit';
import {observer} from "mobx-react";

@observer
class DevelopmentIndexPage extends React.Component {

    render () {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="py-5 text-center">
                        <h1>Расчет стоимости разработки ПО</h1>
                    </div>
                    <div className="text-center mt-2">
                        <Button className='btn-primary btn-lg' to='/development-index/worker-rate'>
                            Dalee
                        </Button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default DevelopmentIndexPage;