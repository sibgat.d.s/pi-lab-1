import React from 'react';
import moment from 'moment';
import { observer } from "mobx-react";
import { Button } from "uikit";

import Store from './data/Store';
import Service from './data/Service';
import { CountStore } from "stores";


@observer
class WorkingCalendarPage extends React.Component {

    constructor (props) {
        super( props );
        console.log(Store.dates)
    }

    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h3>График и этапы разработки проекта</h3>
                </div>
                <div className="card-body">
                    <table className='table'>
                        <thead>
                        <tr>
                            <th>Содержание работы</th>
                            <th>Исполнители</th>
                            <th>Длительность, дни</th>
                            <th>Начало работы</th>
                            <th>Конец работы</th>
                        </tr>
                        </thead>
                        <tbody>
                            {Store.shedule.map(item => {
                                return (
                                    <tr key={item.id}>
                                        <td>{item.name}</td>
                                        <td>
                                            {CountStore.executers.map(exec => {
                                                return(
                                                    <div key={exec.id}>{exec.name}</div>
                                                )
                                            })}
                                        </td>
                                        <td>
                                            {item.durationDays.map(item => {
                                                return (
                                                    <div className='text-center' key={item}>{item}</div>
                                                )
                                            })}
                                        </td>
                                        <td>
                                            <div>{Store.workingDates[item.id][0].startPeriod}</div>
                                            <div>{Store.workingDates[item.id][1].startPeriod}</div>
                                        </td>
                                        <td>
                                            <div>{Store.workingDates[item.id][0].endPeriod}</div>
                                            <div>{Store.workingDates[item.id][1].endPeriod}</div>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="row">
                    <div>
                        <p className='m-5'>
                            Окончание разработки проекта будет
                            <span className='mx-1'>
                                {
                                    moment(CountStore.startProjectDate)
                                        .add(Store.daysFromStart[Store.daysFromStart.length - 1], 'days')
                                        .format('LL')
                                }
                            </span>
                        </p>
                    </div>
                </div>
                <div className="row m-4">
                    <Button
                        className='btn-primary'
                        to='/development-index'
                    >
                        Далее
                    </Button>
                </div>
            </div>
        )
    }
}

export default WorkingCalendarPage;