import { computed, observable } from 'mobx';
import lodash from 'lodash';
import moment from 'moment';

import { CountStore } from 'stores';
// import Service from './Service';


class Store {

    @computed
    get shedule() {
        return [
            {
                id: 0,
                name: 'Постановка задачи',
                executers: CountStore.executers,
                durationDays: [1, 3],
            },
            {
                id: 1,
                name: 'Сбор исходных данных',
                executers: CountStore.executers,
                durationDays: [5, 14],
            },
            {
                id: 2,
                name: 'Анализ существующих методов решения задачи и программных средств',
                executers: CountStore.executers,
                durationDays: [0, 6],
            },
            {
                id: 3,
                name: 'Обоснование принципиальной необходимости разработки',
                executers: CountStore.executers,
                durationDays: [1, 2],
            },
            {
                id: 4,
                name: 'Определение и анализ требований к программе',
                executers: CountStore.executers,
                durationDays: [1, 3],
            },
            {
                id: 5,
                name: 'Определение структуры входных и выходных данных',
                executers: CountStore.executers,
                durationDays: [1, 5],
            },
            {
                id: 6,
                name: 'Выбор технических средств и программных средств реализации',
                executers: CountStore.executers,
                durationDays: [1, 3],
            },
            {
                id: 7,
                name: 'Проектирование программной архитектуры',
                executers: CountStore.executers,
                durationDays: [0, 3],
            },
            {
                id: 8,
                name: 'Техническое проектирование компонентов программы',
                executers: CountStore.executers,
                durationDays: [0, 7],
            },
            {
                id: 9,
                name: 'Программирование модулей в выбранной среде программирования',
                executers: CountStore.executers,
                durationDays: [0, 13],
            },
            {
                id: 10,
                name: 'Тестирование программных модулей',
                executers: CountStore.executers,
                durationDays: [0, 21],
            },
            {
                id: 11,
                name: 'Сборка и испытание программы',
                executers: CountStore.executers,
                durationDays: [2, 5],
            },
            {
                id: 12,
                name: 'Анализ результатов испытаний',
                executers: CountStore.executers,
                durationDays: [1, 5],
            },
            {
                id: 13,
                name: 'Проведение расчетов показателей безопасности жизнедеятельности',
                executers: CountStore.executers,
                durationDays: [0, 3],
            },
            {
                id: 14,
                name: 'Проведение экономических расчетов',
                executers: CountStore.executers,
                durationDays: [0, 4],
            },
            {
                id: 15,
                name: 'Оформление пояснительной записки',
                executers: CountStore.executers,
                durationDays: [5, 15],
            },
        ];
    }

    @observable daysFromStart = [0];

    @computed
    get workingDates() {
        const result = [];
        this.shedule.map((item, index) => {
            this.daysFromStart.push(item.durationDays[0] > item.durationDays[1]
                ? this.daysFromStart[index] + item.durationDays[0] + 1
                : this.daysFromStart[index] + item.durationDays[1] + 1);
            result.push([
                {
                    startPeriod: item.durationDays[0] ? moment(CountStore.startProjectDate)
                            .add(this.daysFromStart[index], 'days')
                            .format('ll') : '_',
                    endPeriod: item.durationDays[0] ? moment(CountStore.startProjectDate)
                            .add(this.daysFromStart[index] + item.durationDays[0], 'days')
                            .format('ll') : '_',
                },
                {
                    startPeriod: item.durationDays[1] ? moment(CountStore.startProjectDate)
                            .add(this.daysFromStart[index], 'days')
                            .format('ll') : '_',
                    endPeriod: item.durationDays[1] ? moment(CountStore.startProjectDate)
                            .add(this.daysFromStart[index] + item.durationDays[1], 'days')
                            .format('ll') : '_',
                }
            ]);
        });
        return result;
    }
}




const store = new Store();
export default store;