import moment from 'moment';

class Service {
    getWorkingDate(params) {
        let workingDate = moment(params.date).isoWeekday() === 7
            ? moment(params.date).add(1, 'days')
            : moment(params.date);
        let workingDays = params.days || 0;
        for (let i = 0; i < workingDays; i++) {
            if (workingDate.isoWeekday() === 6) {
                workingDays += 2;
            }
            workingDate.add(1, 'days');
        }
        return workingDate;
    }
}

const service = new Service();
export default service;