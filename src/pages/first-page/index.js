import React from 'react';

import Store from './data/Store';
import {CountStore} from 'stores'
import {CountService} from 'services';

import {
    Card,
    Button,
    Input,
    Checkbox,
} from 'uikit';
import {action} from "mobx";
import {observer} from "mobx-react";

@observer
class FirstPage extends React.Component {

    @action
    handleChange = (e) => {
        Store[e.name] = e.value;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (Store.QIName.trim() !== '') {
            CountStore.qualityIndicators.push({
                id: CountStore.qualityIndicators.length + 1,
                name: Store.QIName,
                isNeeded: 1,
                value: 0,
            });
        }
        Store.QIName = '';
        Store.toAddQI = false;
    };

    handleAddedQI = () => {
        Store.toAddQI = true;
    };

    render() {
        return (
            <React.Fragment>
                <div className="py-2 px-2">
                    <div className="">
                        <Card
                            className='mt-2'
                            label='Выберите показатели качества или добавьте свои'
                        >
                            {
                                CountStore.qualityIndicators.map(item => {
                                    return (
                                        <Checkbox
                                            key={item.id}
                                            label={item.name}
                                            name={+item.id - 1}
                                            value={item.isNeeded}
                                            onClick={CountService.handleChangeQualityIndicators}
                                        />
                                    )
                                })
                            }
                            {
                                Store.toAddQI == false
                                    ? (
                                        <Button
                                            className="btn-outline-secondary btn-sm mt-2"
                                            onClick={this.handleAddedQI}
                                        >
                                            +
                                        </Button>
                                    )
                                    : (
                                        <form onSubmit={this.handleSubmit}>
                                            <Input
                                                name='QIName'
                                                onChange={this.handleChange}
                                                value={Store.QIName}
                                            />
                                        </form>
                                    )
                            }
                        </Card>
                    </div>
                    <Button
                        className="btn-primary mt-2"
                        to='/ctp-calculation-page'
                    >
                        Далее
                    </Button>
                </div>
            </React.Fragment>
        )
    }
}

export default FirstPage;