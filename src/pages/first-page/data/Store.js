import {
    observable
} from "mobx";

class Store {
    @observable isProectingCost = false;
    @observable isRealiseCost = false;
    @observable toAddQI = false;
    @observable QIName = '';
}

const store = new Store();
export default store;