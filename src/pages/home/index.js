import React from 'react';

import {
    Button
} from 'uikit';
import {observer} from "mobx-react";

@observer
class HomePage extends React.Component {

    render () {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="py-5 text-center">
                        <h1>Программа для расчсета времени разработки ПО</h1>
                    </div>
                    <div className="text-center mt-2">
                        <Button className='btn-primary btn-lg' to='/first-page'>
                            Dalee
                        </Button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default HomePage;