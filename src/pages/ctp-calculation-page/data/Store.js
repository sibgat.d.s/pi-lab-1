import {
    observable,
    computed
} from "mobx";
import lodash from 'lodash';

class Store {
    @observable developersCount = '';
    @observable needCoefMeaning = false;

    @computed
    get options() {
        return lodash.range(1, 6).map(item => {
            return {
                id: item,
                name: item,
            }
        })
    }
}

const store = new Store();
export default store;