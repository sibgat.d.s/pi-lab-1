import React from 'react';
import {action} from "mobx";
import {observer} from "mobx-react";
import lodash from 'lodash';

import {
    Input,
    Button,
    Select
} from 'uikit';

import Store from './data/Store';
import {CountStore} from 'stores'
import {CountService} from 'services';



@observer
class CtpCalculationPage extends React.Component {

    constructor(props) {
        super(props);
    }

    handleCalculateCoef = () => {
        const J1 = lodash.sumBy(CountStore.QIRealised, item => {
            return item.value * item.expertRatingProj;
        })
        const J2 = lodash.sumBy(CountStore.QIRealised, item => {
            return item.value * item.expertRatingAnal;
        })
        CountStore.CTP = Math.round(J1 / J2 * 100) / 100;
    }

    handleShowCoefMeaning = () => {
        Store.needCoefMeaning = true;
    }


    @action
    handleChange = (e) => {
        Store[e.name] = e.value;
    };

    componentDidMount = () => {
        let index = 0;
        CountStore.QIRealised = CountStore.qualityIndicators.filter((item) => {
            let newItem = item;
            if (newItem.isNeeded) {
                newItem.id = index++;
                newItem.expertRatingProj = 3;
                newItem.expertRatingAnal = 3;
                return newItem;
            }

        });
        CountStore.QIRealised.map(item => {
            item.value = (1 / CountStore.QIRealised.length).toFixed(2);
        })

    };

    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h3>Подсчет рентабильности разработки проекта, по сравнению с аналогом</h3>
                </div>
                <div className="card-body">
                    <table className='table'>
                        <thead>
                        <tr>
                            <th>QI name</th>
                            <th>value(<span className='text-muted'>В сумме не больше 1</span>)</th>
                            <th>This project(<span className='text-muted'>Значение от 1 до 5</span>)</th>
                            <th>Analog(<span className='text-muted'>Значение от 1 до 5</span>)</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            CountStore.QIRealised.map(item => {
                                if (item.isNeeded) {
                                    return (
                                        <tr key={item.id}>
                                            <th>
                                                {item.name}
                                            </th>
                                            <th>
                                                <Input
                                                    value={item.value}
                                                    id={item.id}
                                                    name='value'
                                                    onChange={CountService.handleChangeQIRealised}
                                                    required
                                                    placeholder='Введите от 0 до 1...'
                                                />
                                            </th>
                                            <th>
                                                <Select
                                                    value={item.expertRatingProj}
                                                    id={item.id}
                                                    name='expertRatingProj'
                                                    options={Store.options}
                                                    onChange={CountService.handleChangeQIRealised}
                                                    required
                                                    placeholder='Введите от 1 до 5...'
                                                />
                                            </th>
                                            <th>
                                                <Select
                                                    value={item.expertRatingAnal}
                                                    id={item.id}
                                                    name='expertRatingAnal'
                                                    options={Store.options}
                                                    onChange={CountService.handleChangeQIRealised}
                                                    required
                                                    placeholder='Введите от 1 до 5...'
                                                />
                                            </th>
                                        </tr>
                                    )
                                }
                            })
                        }
                        </tbody>
                    </table>
                </div>
                <div className="row">
                    <div className="col-7 m-2">
                        <Button
                            className='mr-2 btn-outline-primary'
                            onClick={this.handleCalculateCoef}
                        >
                            Посчитать коэффициент тех. совершенства
                        </Button>
                        <Button
                            className='btn-primary'
                            to='/working-calendar'
                        >
                            Далее
                        </Button>
                    </div>
                </div>
                {
                    (CountStore.CTP) && (
                        <div className="row m-2">
                            <div className="col-7 m-2">
                                <h3>Коэффициент технического совершенства продукта:
                                    <span className='text-black-50'>
                                            {CountStore.CTP}
                                        </span>
                                </h3>
                            </div>
                            {
                                (!Store.needCoefMeaning) ? (
                                    <div className="col-5 align-self-right">
                                        <Button
                                            className='mr-2 btn-outline-primary'
                                            onClick={this.handleShowCoefMeaning}
                                        >
                                            Показать значение коэффициента
                                        </Button>
                                    </div>
                                ) : (
                                    <p className='text-muted'>
                                        Значение коэффициента показывает оправданность разработки
                                        проекта с техниеской точки зрения, поэтому если коэффициент больше
                                        единицы, значит разработка такого проекта оправдана.
                                    </p>
                                )
                            }
                        </div>
                    )
                }
            </div>
        )
    }
}

export default CtpCalculationPage;