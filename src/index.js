import React from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import "./styles.scss";
import 'bootstrap/dist/css/bootstrap.min.css';
import Router from './router';

class App extends React.Component {
    render () {
        return (
            <Router/>
        );
    }
}

let mountNode = document.getElementById("app");
ReactDOM.render(<App />, mountNode);