const path = require('path');

const resolve = {
    extensions: [
        '*',
        '.js',
        '.jsx'
    ],
    alias: {
        stores: path.resolve(__dirname, './src/shared/stores/'),
        services: path.resolve(__dirname, './src/shared/services/'),
        uikit: path.resolve(__dirname, './src/shared/uikit/'),
        utilities: path.resolve(__dirname, './src/shared/utilities'),
        models: path.resolve(__dirname, './src/shared/models'),
    }
}

module.exports = resolve;


